import './App.css';
import ComponentName from './ComponentName';

function App() {

  return (
    <ComponentName country="Finland"></ComponentName>
  );
}


export default App;
